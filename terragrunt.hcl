remote_state {
  backend = "s3"
  config = {
    bucket = "tc-terragrunt-state"

    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = "us-east-2"
    encrypt        = true
    dynamodb_table = "tc-terragrunt-state-lock"
  }
}
