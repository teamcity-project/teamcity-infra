# Teamcity demo project

Infrastructure repository for install Teamcity server and Teamcity agents to AWS ECS with Terraform / Terragrunt (for demo purpose only)

The proposed configuration to be able to create listed resources:
- VPC with multiple AZs, two subnets (public and private), and single NAT gateway by default
- Bastion (aka 'jump') host for accessing the internal resources from external network
- ALB for external access to Teamcity server
- SSL certificate for Teamcity custom domain name with auto-validation via Route53 (optional)
- RDS (PostgreSQL) as DB for Teamcity server
- EFS for shared data directory of Teamcity server
- Two ECS clusters (for Teamcity server and agents)
- ECS services on separate ECS clusters for Teamcity server (single installation, without HA) and agents
- Monitoring ang logging via CloudWatch and CloudWatch Logs

## Repository structure
```
├── terragrunt.hcl                                 # S3, DynamoDB, region settings
└── us-east-2                                      # Regional resources
    └── prod                                       # Environment
        ├── common_vars.yaml                       # Variables shared by all resources in the environment
        ├── ecs-cluster                            # Logical group (for example, ECS cluster)
        │   ├── teamcity-agent                     # Resource in the group
        │   │   └── terragrunt.hcl
        │   └── teamcity-server
        │       └── terragrunt.hcl
        │
        ├── ecs-service
        │   ├── teamcity-agent
        │   │   └── terragrunt.hcl
        │   └── teamcity-server
        │       └── terragrunt.hcl
        │
        ├── networking
        │   ├── alb
        │   │   └── terragrunt.hcl
        │   └── vpc-app
        │       └── terragrunt.hcl
        │
        ├── security
        │   ├── bastion-host
        │   │   └── terragrunt.hcl
        │   └── cert-manager
        │       └── teamcity-certificate
        │           └── terragrunt.hcl
        │
        └── stateful-services
            ├── efs-tc-server
            │   └── terragrunt.hcl
            └── postgres
                └── terragrunt.hcl
```
## Requirements

* [Terraform](https://www.terraform.io/downloads.html) 0.12.6 ot later (has tested with 0.12.20)
* [Terragrunt](https://terragrunt.gruntwork.io/docs/getting-started/install/) 0.19.0 or later (has tested with 0.21.12)
* Existing key pair for EC2 instances
* S3-backet for storing the Terragrunt state
* DynamoDB for locking access to the Terragrunt state
* IAM account with properly configured IAM policies for creating resources via Terragrunt

## Preparing
1. Export AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY environment variables (```~/.aws/credentials``` or ```~/.aws/config``` may not work properly)
2. (Optional) create directory for storing Terraform plugin cache and export the ```TF_PLUGIN_CACHE_DIR``` for avoiding extremely growing .terragrunt-cache directories
    ```
    mkdir -p $HOME/.terraform.d/plugin-cache
    export TF_PLUGIN_CACHE_DIR="$HOME/.terraform.d/plugin-cache"
    ```
3. Specify correct information about S3-backet and DynamoDB settings (names, path to state files and region) in [terragrunt.hcl](terragrunt.hcl)
4. Set variables in [us-east-2/prod/common_vars.yaml](us-east-2/prod/common_vars.yaml):
    - **region**
    - **environment** (used as Tag and name in many resources)
    - **project** (used as Tag and name in many resources)
    - **db_username** (RDS Postgres username)
    - **db_password** (RDS Postgres password). _Don't keep it at public place (such as this repo, for example)_
    - **ssh_key_pair_name**
    - **ssh_whitelist_cidr** (list of IPs/subnets for accessing to the bastion host)
    - **route53_zone_domain_name** (name of existing Route53 hosted zone for Teamcity custom domain name, optional)
    - **teamcity_server_custom_domain_name** (custom domain name for Teamcity server, optional)
5. Set/change variables for resources:
    - [VPC](us-east-2/prod/networking/vpc-app/terragrunt.hcl) (in accordance with the [wrapper-modules/vpc](https://gitlab.com/teamcity-project/wrapper-modules/vpc) documentation)
    - [bastion-host](us-east-2/prod/security/bastion-host/terragrunt.hcl) ([wrapper-modules/ec2-asg](https://gitlab.com/teamcity-project/wrapper-modules/ec2-asg) module)
    - [teamcity-certificate](us-east-2/prod/security/cert-manager/teamcity-certificate/terragrunt.hcl) ([wrapper-modules/acm](https://gitlab.com/teamcity-project/wrapper-modules/acm) module)
    - [ALB](us-east-2/prod/networking/alb/terragrunt.hcl) ([wrapper-modules/alb](https://gitlab.com/teamcity-project/wrapper-modules/alb) module)
    - [RDS Postgres](us-east-2/prod/stateful-services/postgres/terragrunt.hcl) ([wrapper-modules/rds-postgres](https://gitlab.com/teamcity-project/wrapper-modules/rds-postgres) module)
    - [EFS for Teamcity server](us-east-2/prod/stateful-services/efs-tc-server/terragrunt.hcl) ([wrapper-modules/efs](https://gitlab.com/teamcity-project/wrapper-modules/alb) module)
    - ECS cluster for [Teamcity server](us-east-2/prod/ecs-cluster/teamcity-server/terragrunt.hcl) and [Teamcity agents](us-east-2/prod/ecs-cluster/teamcity-agent/terragrunt.hcl) ([wrapper-modules/ecs-cluster](https://gitlab.com/teamcity-project/wrapper-modules/ecs-cluster) module)
    - ECS services for [Teamcity server](us-east-2/prod/ecs-service/teamcity-server/terragrunt.hcl) and [Teamcity agents](us-east-2/prod/ecs-service/teamcity-agent/terragrunt.hcl) ([wrapper-modules/ecs-service](https://gitlab.com/teamcity-project/wrapper-modules/ecs-service) module)

## Usage
1. Get the changes plan of the whole project (on root of the repository)
    ```
    terragrunt plan-all
    ```
    or view plan of the specific resource (in us-east-2/prod/networking/vpc-app subdirectory, for example)
    ```
    cd us-east-2/prod/networking/vpc-app
    terragrunt plan
    ``` 
2. Apply the changes for the whole project
    ```
    terragrunt apply-all
    ```
    or apply the plan of the specific resource (in us-east-2/prod/networking/vpc-app subdirectory, for example)
    ```
    cd us-east-2/prod/networking/vpc-app
    terragrunt apply
    ``` 
3. Delete resources (be very careful with it)
    ```
    terragrunt destroy-all
    ```

## Known issues
There is no possibility to delete or update existing Capacity Provider for ECS at this moment (with AWS provider v2.48.0)
(see [issue 11287](https://github.com/terraform-providers/terraform-provider-aws/issues/11287) and
[issue 633](https://github.com/aws/containers-roadmap/issues/633)), so Terraform creates this one with random name for every time.


## To Do
* [ ] Multinode installation of Teamcity server 
* [x] Use AWS Systems Manager or Secret Manager for storing DB password
* [ ] Access from Teamcity agents to Teamcity server via VPC Endpoint
* [ ] Fix the stucked destroying of ECS cluster
* [ ] Deploy applications with ecs-cli or other good tools
* [x] Use HTTPS on ALB side
* [ ] Add replication instance for RDS
* [ ] Create non-superuser DB application account at RDS for Teamcity server via init-container
* [ ] Implement backup for RDS and Teamcity server
* [ ] Create the SSH pair with Terraform
* [ ] Use separate security group for internal SSH-access
* [ ] Use launch templates for ASG pool with different EC2 types
* [ ] Container images cleanup 
* [ ] Use placement constraints for ECS services
* [ ] Add test environment
* [ ] Add NLB for external SSH-access
* [ ] Move resources to directory with global resources
* [ ] Add IAM auth for RDS
* [ ] Check blue/green and canary deploy with ECS
* [x] Use non-root user for launch Teamcity server
