include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/teamcity-project/wrapper-modules/ecs-service.git?ref=v0.3.0"
}

locals {
  common_vars = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))
  region      = local.common_vars.region
  environment = local.common_vars.environment
  project     = local.common_vars.project
}

dependency "vpc-app" {
  config_path = "../../networking/vpc-app"

  mock_outputs = {
    private_subnets = ["subnet-00000000000000000", "subnet-11111111111111111", "subnet-22222222222222222"]
    vpc_id          = "vpc-00000000000000000"
    azs             = ["re-central-0a", "re-central-0b", "re-central-0c"]
  }
}

dependency "teamcity-agent-cluster" {
  config_path = "../../ecs-cluster/teamcity-agent"

  mock_outputs = {
    ecs_cluster_arn            = "arn:aws:ecs:us-east-2:000000000000:cluster/clu"
    ecs_cluster_name           = "clu"
    ecs_capacity_provider_name = "teamcity-agents-cap-0000000000000000-00000000000000000000000000"
  }
}

dependency "teamcity-alb" {
  config_path = "../../networking/alb"

  mock_outputs = {
    alb_target_group_arns = ["arn:aws:elasticloadbalancing:us-east-2:000000000000:targetgroup/samble-tg/badcoffeoddfo"]
    alb_dns_name          = "elb.amazonaws.com"
  }
}

inputs = {
  region      = local.region
  environment = local.environment

  service_name = "teamcity-agents"
  ecs_cluster  = dependency.teamcity-agent-cluster.outputs.ecs_cluster_arn
  # Workaround to prevent forced service recreating
  capacity_provider_arn = dependency.teamcity-agent-cluster.outputs.ecs_capacity_provider_name
  container_cpu         = 1024
  container_memory      = 512
  //ignore_desired_count_changes = true
  desired_count     = 3
  container_image   = "jetbrains/teamcity-agent"
  container_version = "2019.2.1"

  bind_mount_volumes = [
    {
      name          = "sources"
      host_path     = "/data/buildagent/work"
      containerPath = "/opt/buildagent/work"
    },
    {
      name          = "cache"
      host_path     = "/data/buildagent/systems"
      containerPath = "/opt/buildagent/system"
    }
  ]

  container_env = [{
    "name"  = "SERVER_URL"
    "value" = dependency.teamcity-alb.outputs.alb_dns_name
  }]

  awslogs_region            = local.region
  awslogs_group             = "/ecs/${local.environment}-teamcity-agents"
  awslogs_stream_prefix     = local.project
  awslogs_retention_in_days = 1

  tags = {
    Project     = local.project
    Environment = local.environment
    Cluster     = "teamcity-agents"
  }
}
