include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/teamcity-project/wrapper-modules/ecs-service.git?ref=v0.3.0"
}

locals {
  common_vars = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))
  region      = local.common_vars.region
  environment = local.common_vars.environment
  project     = local.common_vars.project

  # Preventing JSON encode issues with Base64
  startup_script = filebase64("${get_terragrunt_dir()}/files/startup.sh")

  # TODO: To get rid of double shell, so main shell has not PID 1, therefore the process and ECS cluster can't stop properly
  command = [
    "/bin/bash",
    "-c",
    format("%s %s %s", "echo", local.startup_script, "| /usr/bin/base64 -d | /bin/bash")
  ]
}

dependency "vpc-app" {
  config_path = "../../networking/vpc-app"

  mock_outputs = {
    private_subnets = ["subnet-00000000000000000", "subnet-11111111111111111", "subnet-22222222222222222"]
    vpc_id          = "vpc-00000000000000000"
    azs             = ["re-central-0a", "re-central-0b", "re-central-0c"]
  }
}

dependency "teamcity-server-cluster" {
  config_path = "../../ecs-cluster/teamcity-server"

  mock_outputs = {
    ecs_cluster_arn            = "arn:aws:ecs:us-east-2:000000000000:cluster/clu2"
    ecs_cluster_name           = "clu2"
    ecs_capacity_provider_name = "teamcity-server-cap-0000000000000000-00000000000000000000000000"
  }
}

dependency "teamcity-alb" {
  config_path = "../../networking/alb"

  mock_outputs = {
    alb_target_group_arns = ["arn:aws:elasticloadbalancing:us-east-2:000000000000:targetgroup/samble-tg/badcoffeoddfo"]
    alb_dns_name          = "elb.amazonaws.com"
  }
}

dependency "teamcity-postgres" {
  config_path = "../../stateful-services/postgres"

  mock_outputs = {
    rds_username_ssm_parameter_arn = "arn:aws:ssm:us-east-2:000000000000:parameter/F/Q/D/N/master_username"
    rds_password_ssm_parameter_arn = "arn:aws:ssm:us-east-2:000000000000:parameter/F/Q/D/N/master_password"
    postgres_db_instance_address   = "host.us-east-2.rds.amazonaws.com"
    postgres_db_instance_port      = "5432"
    postgres_db_instance_name      = "dbname"
  }
}

inputs = {
  region      = local.region
  environment = local.environment

  service_name          = "teamcity-server"
  alb_target_group_arns = dependency.teamcity-alb.outputs.alb_target_group_arns
  ecs_cluster           = dependency.teamcity-server-cluster.outputs.ecs_cluster_arn
  # Workaround to prevent forced service recreating
  capacity_provider_arn             = dependency.teamcity-server-cluster.outputs.ecs_capacity_provider_name
  health_check_grace_period_seconds = 600
  container_cpu                     = 2048
  container_memory                  = 3096
  // ignore_desired_count_changes = true
  desired_count     = 1
  container_image   = "jetbrains/teamcity-server"
  container_version = "2019.2.1"
  container_port    = 8111
  container_user    = "1000:1000"
  host_port         = 80
  container_command = local.command

  bind_mount_volumes = [
    {
      name          = "data"
      host_path     = "/mnt/efs/teamcity/data"
      containerPath = "/data/teamcity_server/datadir"
    },
    {
      # For single server node only. We should use different log dirs for each server
      name          = "logs"
      host_path     = "/mnt/efs/teamcity/logs"
      containerPath = "/opt/teamcity/logs"
    }
  ]

  container_env = [
    {
      "name"  = "TEAMCITY_SERVER_MEM_OPTS"
      "value" = "-Xmx2g -XX:MaxPermSize=270m -XX:ReservedCodeCacheSize=350m"
    },
    {
      "name"  = "DB_NAME"
      "value" = dependency.teamcity-postgres.outputs.postgres_db_instance_name
    },
    {
      "name"  = "DB_HOST"
      "value" = dependency.teamcity-postgres.outputs.postgres_db_instance_address
    },
    {
      "name"  = "DB_PORT"
      "value" = dependency.teamcity-postgres.outputs.postgres_db_instance_port
    }
  ]

  container_secret_env = [
    {
      "name" : "DB_USERNAME"
      "valueFrom" : dependency.teamcity-postgres.outputs.rds_username_ssm_parameter_arn
    },
    {
      "name" : "DB_PASSWORD"
      "valueFrom" : dependency.teamcity-postgres.outputs.rds_password_ssm_parameter_arn
    }
  ]

  awslogs_region            = local.region
  awslogs_group             = "/ecs/${local.environment}-teamcity-server"
  awslogs_stream_prefix     = local.project
  awslogs_retention_in_days = 1

  tags = {
    Project     = local.project
    Environment = local.environment
    Cluster     = "teamcity-server"
  }
}
