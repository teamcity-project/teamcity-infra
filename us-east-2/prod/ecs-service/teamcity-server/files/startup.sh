#!/usr/bin/env bash

set -e

TC_DATA_DIR="/data/teamcity_server/datadir"
TC_PLUGIN_DIR="${TC_DATA_DIR}"/plugins
TC_JDBC_DIR="${TC_DATA_DIR}"/lib/jdbc
TC_CONFIG_DIR="${TC_DATA_DIR}"/config

mkdir -p "${TC_PLUGIN_DIR}" "${TC_JDBC_DIR}" "${TC_CONFIG_DIR}"

jdbc_drivers=(https://jdbc.postgresql.org/download/postgresql-42.2.10.jar)
teamcity_plugins=(https://teamcity.jetbrains.com/guestAuth/app/rest/builds/buildType:TestDrive_TeamCityAmazonEcsPlugin_Build,tags:release/artifacts/content/aws-ecs.zip \
                  https://raw.githubusercontent.com/FLGMwt/team-city-agent-auto-auth/master/dist/AutoAuthorize.zip)

# Install JDBC drivers
if [ -d "${TC_JDBC_DIR}" ];
then
    for driver in ${jdbc_drivers[@]}
    do
        filename=$(basename ${driver})
        if [ ! -f "${TC_JDBC_DIR}"/"${filename}" ]
        then
            curl -o "${TC_JDBC_DIR}"/"${filename}" -L "${driver}"
            echo Driver "${filename}" downloaded
        fi
    done
fi

# Install plugins
if [ -d "${TC_PLUGIN_DIR}" ];
then
    for plugin in ${teamcity_plugins[@]}
    do
        filename=$(basename ${plugin})
        if [ ! -f "${TC_PLUGIN_DIR}"/"${filename}" ]
        then
            curl -o "${TC_PLUGIN_DIR}"/"${filename}" -L "${driver}"
            echo Plugin "${filename}" downloaded
        fi
    done
fi

# Setup database settings
if [ -z "${DB_USERNAME}" ] || [ -z "${DB_PASSWORD}" ] || [ -z "${DB_NAME}" ] || [ -z "${DB_HOST}" ]  || [ -z "${DB_PORT}" ]
then
    echo "Database settings are not specified"
else
    cat << END_CONFIG > "${TC_CONFIG_DIR}"/database.properties
connectionUrl=jdbc:postgresql://${DB_HOST}:${DB_PORT}/${DB_NAME}
connectionProperties.user=${DB_USERNAME}
connectionProperties.password=${DB_PASSWORD}
END_CONFIG
fi

exec /run-services.sh
