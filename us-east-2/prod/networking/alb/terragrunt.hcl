include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/teamcity-project/wrapper-modules/alb.git?ref=v0.2.0"
}

locals {
  common_vars                        = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))
  region                             = local.common_vars.region
  environment                        = local.common_vars.environment
  project                            = local.common_vars.project
  route53_zone_domain_name           = local.common_vars.route53_zone_domain_name
  teamcity_server_custom_domain_name = local.common_vars.teamcity_server_custom_domain_name
}

dependency "vpc-app" {
  config_path = "../../networking/vpc-app"

  mock_outputs = {
    public_subnets = ["subnet-00000000000000000", "subnet-11111111111111111", "subnet-22222222222222222"]
    vpc_id         = "vpc-00000000000000000"
    azs            = ["re-central-0a", "re-central-0b", "re-central-0c"]
  }
}

dependency "teamcity-certificate" {
  config_path = "../../security/cert-manager/teamcity-certificate"

  mock_outputs = {
    acm_certificate_arn = "arn:aws:iam::123456789098:teamcity-certificate/tc_cert-123456789098"
  }
}

inputs = {
  region                   = local.region
  environment              = local.environment
  route53_zone_domain_name = local.route53_zone_domain_name
  additional_alb_dns_name  = local.teamcity_server_custom_domain_name
  vpc_id                   = dependency.vpc-app.outputs.vpc_id
  subnet_ids               = dependency.vpc-app.outputs.public_subnets
  acm_certificate_arn      = dependency.teamcity-certificate.outputs.acm_certificate_arn

  alb_name             = "teamcity"
  deregistration_delay = 10

  # backend port will be overriden after creating ECS services
  backend_port = 80

  health_check = {
    interval            = 5
    path                = "/login.html"
    port                = "traffic-port"
    healthy_threshold   = 2
    unhealthy_threshold = 3
    timeout             = 4
    protocol            = "HTTP"
    matcher             = "200-399"
  }

  tags = {
    Project     = local.project
    Environment = local.environment
  }
}
