include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/teamcity-project/wrapper-modules/vpc.git?ref=v0.1.0"
}

locals {
  common_vars = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))
  region      = local.common_vars.region
  environment = local.common_vars.environment
  project     = local.common_vars.project
}

inputs = {
  suffix                       = "app"
  region                       = local.region
  environment                  = local.environment
  cidr                         = "10.100.0.0/16"
  azs_count                    = 3
  create_database_subnet_group = false
  enable_dns_hostnames         = true
  enable_dns_support           = true
  enable_nat_gateway           = true
  single_nat_gateway           = true
  one_nat_gateway_per_az       = false
  enable_dhcp_options          = false

  tags = {
    Project     = local.project
    Environment = local.environment
  }
}
