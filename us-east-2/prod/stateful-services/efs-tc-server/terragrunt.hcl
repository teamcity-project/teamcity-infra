include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/teamcity-project/wrapper-modules/efs.git?ref=v0.1.0"
}

locals {
  common_vars = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))
  region      = local.common_vars.region
  environment = local.common_vars.environment
  project     = local.common_vars.project
}

dependency "vpc-app" {
  config_path = "../../networking/vpc-app"

  mock_outputs = {
    private_subnets = ["subnet-00000000000000000", "subnet-11111111111111111", "subnet-22222222222222222"]
    vpc_id          = "vpc-00000000000000000"
    azs             = ["re-central-0a", "re-central-0b", "re-central-0c"]
  }
}

inputs = {
  region      = local.region
  environment = local.environment
  vpc_id      = dependency.vpc-app.outputs.vpc_id
  efs_name    = "efs-tc-server"
  subnet_ids  = dependency.vpc-app.outputs.private_subnets

  tags = {
    Project     = local.project
    Environment = local.environment
    Cluster     = "teamcity-server"
  }
}
