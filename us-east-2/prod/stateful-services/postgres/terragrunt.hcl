include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/teamcity-project/wrapper-modules/rds-postgres.git?ref=v0.2.0"
}

locals {
  common_vars = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))
  region      = local.common_vars.region
  environment = local.common_vars.environment
  project     = local.common_vars.project
  # It's very insecure way!
  db_username = local.common_vars.db_username
  db_password = local.common_vars.db_password
}

dependency "vpc-app" {
  config_path = "../../networking/vpc-app"

  mock_outputs = {
    private_subnets = ["subnet-00000000000000000", "subnet-11111111111111111", "subnet-22222222222222222"]
    vpc_id          = "vpc-00000000000000000"
    azs             = ["re-central-0a", "re-central-0b", "re-central-0c"]
  }
}

inputs = {
  region     = local.region
  subnet_ids = dependency.vpc-app.outputs.private_subnets
  vpc_id     = dependency.vpc-app.outputs.vpc_id

  ssm_paramter_prefix = "/${local.project}/${local.environment}/"

  pg_family            = "postgres11"
  engine_version       = "11.5"
  major_engine_version = "11"
  instance_class       = "db.t2.micro"
  allocated_storage    = "50"
  storage_type         = "gp2"
  storage_encrypted    = false

  db_name            = "teamcity"
  master_db_username = local.db_username

  # To avoid storing your DB password in the code, set it as the environment variable TF_VAR_master_db_password
  master_db_password = local.db_password

  pg_port = "5432"

  pg_parameters = [
    {
      name  = "client_encoding"
      value = "utf8"
    },
    {
      # For PostgreSQL version 10 and later, max_wal_size is in units of 1 MB
      name         = "max_wal_size"
      value        = "1500"
      apply_method = "pending-reboot"
    },
    {
      name  = "checkpoint_completion_target"
      value = "0.9"
    },
    {
      # 256 MiB in units of 8k pages
      name         = "shared_buffers"
      value        = "32768"
      apply_method = "pending-reboot"
    }
  ]

  vpc_security_group_ids = []

  availability_zone = sort(dependency.vpc-app.outputs.azs)[0]

  backup_retention_period = 0
  backup_window           = "02:00-04:00"
  maintenance_window      = "Mon:00:00-Mon:02:00"
  deletion_protection     = false

  use_ssm_parameter = true

  tags = {
    Project     = local.project
    Environment = local.environment
  }
}
