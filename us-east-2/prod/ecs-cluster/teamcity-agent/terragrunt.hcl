include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/teamcity-project/wrapper-modules/ecs-cluster.git?ref=v0.3.0"
}

locals {
  common_vars       = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))
  region            = local.common_vars.region
  environment       = local.common_vars.environment
  project           = local.common_vars.project
  ssh_key_pair_name = local.common_vars.ssh_key_pair_name
}

dependency "vpc-app" {
  config_path = "../../networking/vpc-app"

  mock_outputs = {
    private_subnets = ["subnet-00000000000000000", "subnet-11111111111111111", "subnet-22222222222222222"]
    vpc_id          = "vpc-00000000000000000"
    azs             = ["re-central-0a", "re-central-0b", "re-central-0c"]
  }
}

inputs = {
  region                         = local.region
  environment                    = local.environment
  ssh_key_pair_name              = local.ssh_key_pair_name
  vpc_id                         = dependency.vpc-app.outputs.vpc_id
  subnet_ids                     = dependency.vpc-app.outputs.private_subnets
  vpc_security_group_ids         = [dependency.vpc-app.outputs.default_security_group_id]
  ec2_instance_type              = "t3a.micro"
  spot_price                     = 0.0028
  asg_max_size                   = 3
  cluster_name                   = "teamcity-agents"
  managed_termination_protection = "DISABLED"

  cluster_settings = [{
    name  = "containerInsights"
    value = "enabled"
  }]

  tags = {
    Project     = local.project
    Environment = local.environment
    Cluster     = "teamcity-agents"
  }
}
