include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/teamcity-project/wrapper-modules/ecs-cluster.git?ref=v0.3.0"
}

locals {
  common_vars       = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))
  region            = local.common_vars.region
  environment       = local.common_vars.environment
  project           = local.common_vars.project
  ssh_key_pair_name = local.common_vars.ssh_key_pair_name
}

dependency "teamcity-alb" {
  config_path = "../../networking/alb"

  mock_outputs = {
    alb_security_group_id = "sg-badcoffeeioddfood"
  }
}

dependency "vpc-app" {
  config_path = "../../networking/vpc-app"

  mock_outputs = {
    private_subnets = ["subnet-00000000000000000", "subnet-11111111111111111", "subnet-22222222222222222"]
    vpc_id          = "vpc-00000000000000000"
    azs             = ["re-central-0a", "re-central-0b", "re-central-0c"]
  }
}

dependency "teamcity-postgres" {
  config_path = "../../stateful-services/postgres"

  mock_outputs = {
    rds_security_group_id = "sg-00000000000000000"
  }
}

dependency "efs-tc-server" {
  config_path = "../../stateful-services/efs-tc-server"

  mock_outputs = {
    dns_name          = "host.amazon.com"
    security_group_id = "sg-00000000000000000"
  }
}

inputs = {
  region                 = local.region
  environment            = local.environment
  ssh_key_pair_name      = local.ssh_key_pair_name
  vpc_id                 = dependency.vpc-app.outputs.vpc_id
  subnet_ids             = dependency.vpc-app.outputs.private_subnets
  alb_security_group_id  = dependency.teamcity-alb.outputs.alb_security_group_id
  vpc_security_group_ids = [dependency.efs-tc-server.outputs.security_group_id, dependency.vpc-app.outputs.default_security_group_id, dependency.teamcity-postgres.outputs.rds_security_group_id]
  ec2_instance_type      = "t3a.medium"
  spot_price             = 0.0113
  asg_max_size           = 1
  cluster_name           = "teamcity-server"
  efs_servername         = dependency.efs-tc-server.outputs.dns_name
  ecs_secrets_support    = true
  postinstall_user_script = "mkdir -p /mnt/efs/teamcity/data /mnt/efs/teamcity/logs; chown -R 1000:1000 /mnt/efs/teamcity"

  cluster_settings = [{
    name  = "containerInsights"
    value = "enabled"
  }]

  tags = {
    Project     = local.project
    Environment = local.environment
    Cluster     = "teamcity-server"
  }
}
