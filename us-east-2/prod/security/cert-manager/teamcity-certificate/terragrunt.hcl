include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/teamcity-project/wrapper-modules/acm.git?ref=v0.1.0"
}

locals {
  common_vars                        = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))
  region                             = local.common_vars.region
  environment                        = local.common_vars.environment
  project                            = local.common_vars.project
  route53_zone_domain_name           = local.common_vars.route53_zone_domain_name
  teamcity_server_custom_domain_name = local.common_vars.teamcity_server_custom_domain_name
}

inputs = {
  region                   = local.region
  environment              = local.environment
  route53_zone_domain_name = local.route53_zone_domain_name
  certificate_domain_name  = local.teamcity_server_custom_domain_name

  use_existing_route53_zone = true
  validate_certificate      = true
  wait_for_validation       = true

  tags = {
    Name        = local.teamcity_server_custom_domain_name
    Project     = local.project
    Environment = local.environment
  }
}
