include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::https://gitlab.com/teamcity-project/wrapper-modules/ec2-asg.git?ref=v0.1.0"
}

locals {
  common_vars        = yamldecode(file("${get_terragrunt_dir()}/${find_in_parent_folders("common_vars.yaml")}"))
  region             = local.common_vars.region
  environment        = local.common_vars.environment
  project            = local.common_vars.project
  ssh_key_pair_name  = local.common_vars.ssh_key_pair_name
  ssh_whitelist_cidr = local.common_vars.ssh_whitelist_cidr
}

dependency "vpc-app" {
  config_path = "../../networking/vpc-app"

  mock_outputs = {
    public_subnets = ["subnet-00000000000000000", "subnet-11111111111111111", "subnet-22222222222222222"]
    vpc_id         = "vpc-00000000000000000"
    azs            = ["re-central-0a", "re-central-0b", "re-central-0c"]
  }
}

inputs = {
  region                       = local.region
  environment                  = local.environment
  ssh_key_pair_name            = local.ssh_key_pair_name
  ssh_whitelist_cidr           = local.ssh_whitelist_cidr
  ssh_vpc_id                   = dependency.vpc-app.outputs.vpc_id
  subnet_ids                   = dependency.vpc-app.outputs.public_subnets
  vpc_security_group_ids       = [dependency.vpc-app.outputs.default_security_group_id]
  asg_name                     = "bastion"
  ec2_instance_type            = "t3.nano"
  spot_price                   = 0.0016
  asg_max_size                 = 1
  associate_public_ip_address  = true
  recreate_asg_when_lc_changes = true

  tags = {
    Project     = local.project
    Environment = local.environment
  }
}
